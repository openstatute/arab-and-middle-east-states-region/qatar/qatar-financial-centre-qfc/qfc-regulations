QFC Law No. (7) of Year 2005

https://qfcra-en.thomsonreuters.com/rulebook/qfc-law-no-7-year-2005 

Click here to view the PDF version of Law as amended.
Click here to view the Word version of Law as amended.

Click here to view earlier versions of QFC Law No. (7) of 2005.
Click here to view the PDF version of the Law No.(7) of 2005 as made in Arabic.

Click here to view the PDF version of the Law No.(2) of 2009.
Click here to view the Word version of the Law No.(2) of 2009.
Click here to view the PDF version of the Law No.(2) of 2009 as made in Arabic.

Click here to view the PDF version of the Law No.(14) of 2009.
Click here to view the Word version of the Law No.(14) of 2009.
Click here to view the PDF version of the Law No.(14) of 2009 as made in Arabic.
QFCA Law No. (7) of Year 2005
Article 1: 	Definitions
Article 2: 	QFC — Location
Article 3: 	QFC Authority
Article 4: 	QFC Board
Article 5: 	Objectives of the QFC Authority
Article 6: 	Powers of the QFC Authority
Article 7: 	The QFC Companies Registration Office
Article 8: 	The Regulatory Authority, The Regulatory Tribunal and The Civil and Commercial Court
Article 9: 	Power to make regulations
Article 10: 	Permitted Activities within QFC
Article 11: 	Licensing of operations
Article 12: 	Statutory guarantees
Article 13: 	Revenue of the QFC Authority
Article 14: 	Treatment of Surpluses
Article 15: 	Accounting Requirements
Article 16: 	Liability of QFC Authority, The Regulatory Authority, The Regulatory Tribunal, The Civil and Commercial Court and QFC Institutions
Article 17: 	Taxation
Article 18: 	Interaction with other laws
Article 19: 	Miscellaneous
Schedule 1 - 	Board — Constitution and Powers, Chairman and Director General
Schedule 2 - 	Regulations
Schedule 3 - 	Permitted Activities
Schedule 4 - 	The Regulatory Authority
Schedule 5 - 	The Regulatory Tribunal
Schedule 6 - 	The Civil and Commercial Court


[**‹** Qatar Financial Centre Legislation](/rulebook/qatar-financial-centre-legislation "Go to previous page")

[Article 1 – Definitions **›**](/rulebook/article-1-definitions "Go to next page")

QFC Law No. (7) of Year 2005
----------------------------

Click **[here](/sites/default/files/net_file_store/QFC_Law-V3-Oct09.doc.pdf)** to view the PDF version of Law as amended.  
Click **[here](/sites/default/files/net_file_store/QFC_Law-V3-Oct09_2.doc)** to view the Word version of Law as amended.

Click **[here](/node/6769)** to view earlier versions of QFC Law No. (7) of 2005.  
Click **[here](/sites/default/files/net_file_store/QFC_Law_No7_of_2005.pdf)** to view the PDF version of the Law No.(7) of 2005 as made in Arabic.

Click **[here](/sites/default/files/net_file_store/QFC_Law_No2_of_2009.pdf)** to view the PDF version of the Law No.(2) of 2009.  
Click **[here](/sites/default/files/net_file_store/QFC_Law_No.2_of_2009.doc)** to view the Word version of the Law No.(2) of 2009.  
Click **[here](/sites/default/files/net_file_store/QFC_Law_No.2_of_2009_Arabic.pdf)** to view the PDF version of the Law No.(2) of 2009 as made in Arabic.

Click **[here](/sites/default/files/net_file_store/Law_No._14_of_2009.pdf)** to view the PDF version of the Law No.(14) of 2009.  
Click **[here](/sites/default/files/net_file_store/Law_No_14_of_2009Z.doc)** to view the Word version of the Law No.(14) of 2009.  
Click **[here](/sites/default/files/net_file_store/QFC_Law_No.14_of_2009_Arabic.pdf)** to view the PDF version of the Law No.(14) of 2009 as made in Arabic.

**QFCA Law No. (7) of Year 2005**

[Article 1](/node/1594):

**Definitions**

[Article 2](/node/1595):

**QFC — Location**

[Article 3](/node/1596):

**QFC Authority**

[Article 4](/node/1597):

**QFC Board**

[Article 5](/node/1598):

**Objectives of the QFC Authority**

[Article 6](/node/1599):

**Powers of the QFC Authority**

[Article 7](/node/1600):

**The QFC Companies Registration Office**

[Article 8](/node/1601):

**The Regulatory Authority, The Regulatory Tribunal and The Civil and Commercial Court**

[Article 9](/node/1602):

**Power to make regulations**

[Article 10](/node/1603):

**Permitted Activities within QFC**

[Article 11](/node/1604):

**Licensing of operations**

[Article 12](/node/1605):

**Statutory guarantees**

[Article 13](/node/1606):

**Revenue of the QFC Authority**

[Article 14](/node/1607):

**Treatment of Surpluses**

[Article 15](/node/1608):

**Accounting Requirements**

[Article 16](/node/1609):

**Liability of QFC Authority, The Regulatory Authority, The Regulatory Tribunal, The Civil and Commercial Court and QFC Institutions**

[Article 17](/node/1610):

**Taxation**

[Article 18](/node/1611):

**Interaction with other laws**

[Article 19](/node/1612):

**Miscellaneous**

[Schedule 1](/node/1613) -

**Board — Constitution and Powers, Chairman and Director General**

[Schedule 2](/node/1614) -

**Regulations**

[Schedule 3](/node/1615) -

**Permitted Activities**

[Schedule 4](/node/1616) -

**The Regulatory Authority**

[Schedule 5](/node/1617) -

**The Regulatory Tribunal**

[Schedule 6](/node/6747) -

**The Civil and Commercial Court**

[**‹** Qatar Financial Centre Legislation](/rulebook/qatar-financial-centre-legislation "Go to previous page")

[Article 1 – Definitions **›**](/rulebook/article-1-definitions "Go to next page")

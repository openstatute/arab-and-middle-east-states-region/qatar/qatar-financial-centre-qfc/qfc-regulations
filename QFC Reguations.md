QFC Regulations
Source: https://qfcra-en.thomsonreuters.com/rulebook/qfc-regulations

    Arbitration Regulations 2005
        Enactment Notice
        Part 1: Application, Interpretation and Commencement
        Part 2: Scope of Application
        Part 3: Arbitration
        Part 4: The Recognition and Enforcement of Non-QFC Awards
        Part 5: Interpretation and Definitions
    Companies Regulations 2005
        Enactment Notice
        Part 1: Application, Commencement and Interpretation
        Part 2: Companies Registration Office
        Part 3: Limited Liability Companies
        Part 3A: Companies Limited by Guarantee
        Part 4: Protected Cell Companies
        Part 5: Migration of Body Corporate
        Part 6: Branches
        Part 7: International Business Companies
        Part 8: Contraventions
        Part 9: Other Provisions Relating to the CRO
        Part 10: Application to the QFC Civil and Commercial Court
        Part 11: Reporting
        Part 12: Register of Financing Statements
        Part 13: Acquisition of Minorities in Take-Overs
        Part 14: Interpretation and Definitions
        Schedule 1: Contraventions with financial penalties stipulated
        Schedule 2: Financing change statement
        Endnotes
    Contract Regulations 2005
        Enactment Notice
        Part 1: Application and Commencement
        Part 2: Nature of Contract
        Part 3: Formation of Contract
        Part 4: Validity
        Part 5: Interpretation
        Part 6: Content
        Part 7: Agency
        Part 8: Performance
        Part 9: Set-Off
        Part 10: Non-Performance
        Part 11: Remedies
        Part 12: Termination
        Part 13: Transfer of Rights and Obligations
        Part 14: Rights of Third Parties
        Part 15: Interpretation and Definitions
    Data Protection Regulations 2021
        Enactment Notice
        Part 1: Application, Commencement and Interpretation
        Part 2: General Provisions for the Processing of Personal Data
        Part 3: Data Subjects' Rights
        Part 4: Transfers of Data Outside the QFC
        Part 5: Data Controller and Data Processor Obligations
        Part 6: The Data Protection Office
        Part 7: Remedies
        Part 8: Final Provisions
    Employment Regulations
        Enactment Notice
        Part 1: Application, Interpretation and Commencement
        Part 2: Employment Standards Office
        Part 3: General
        Part 4: Non-Discrimination
        Part 5: Whistleblowing
        Part 6: Employment Terms
        Part 7: Payment of Salary
        Part 8: Work Hours and Leave
        Part 9: Employment of Women
        Part 10: Health, Safety and Welfare
        Part 11: Work-Related Injuries and Compensation
        Part 12: Investigations and Proceedings
        Part 13: Interpretation and Definitions
    Financial Services Regulations
        Enactment Notice
        Part 1: Application, Commencement and Interpretation
        Part 2: Introduction
        Part 3: The Regulatory Authority
        Part 4: Regulated Activities
        Part 5: Authorisation Requirements and Process
        Part 6: Controllers
        Part 7: Individuals and Controlled Functions
        Part 8: Supervision and Investigations
        Part 9: Disciplinary and Enforcement Powers
        Part 10: Enforcement Procedure
        Part 11: Financial Communications
        Part 12: Market Abuse
        Part 13: Contraventions
        Part 14: Complaints and Compensation
        Part 15: Appointment of Auditors and Actuaries
        Part 16: Control of Business Transfers
        Part 17: Investment Funds
        Part 18: Miscellaneous
        Part 19: Interpretation and Definitions
        Schedule 1: The Regulatory Authority
        Schedule 2: Other Duties, Functions and Powers Conferred on the Regulatory Authority
        Schedule 3: Regulated Activities and Permitted Activities
        Schedule 4: The Regulatory Tribunal
    Foundation Regulations 2016
        Enactment Notice
        Part 1: Introduction
        Part 2: Establishment and Capacity of Foundations
        Part 3: Features of a Foundation
        Part 4: Governing Law of a Foundation
        Part 5: Administration of a Foundation
        Part 6: Provision of Information by a Foundation
        Part 7: Provisions Relating to QFC Authority
        Part 8: Powers of the QFC Court
        Part 9: Interpretation and Definitions
    Immigration Regulations
        Enactment Notice
        Part 1: Application, Commencement and Interpretation
        Part 2: The QFC Immigration Office
        Part 3: Entry and Sponsorship of QFC Employees
        Part 4: Sponsorship
        Part 5: General Provisions
        Part 6: Interpretations and Definitions
    Insolvency Regulations 2005
        Enactment Notice
        PART 1: Application, Commencement and Interpretation
        PART 2: Administration
        PART 3: Winding Up
        PART 4: Provisions Applying to Liquidations Generally
        PART 5: Provisions Relating to Administration, Liquidations and Company Arrangements
        PART 6: Proceedings in Respect of non-QFC Companies
        PART 7: Branches
        PART 8: Other types of Company
        PART 9: Application of the Law to Limited Liability Partnerships
        PART 10: Insolvency Practitioners
        PART 11: Miscellaneous
        PART 12: Financial Markets and Insolvency
        PART 13: Interpretation and Definitions
        SCHEDULE 1: Powers of Liquidator and Administrator
        SCHEDULE 2: Form of Proxy
    Investment Clubs Regulations 2016
        Enactment Notice
        Part 1 Application, Commencement and Interpretation
        Part 2 Companies Registration Office
        Part 3 Limited Liability Company (Investment Clubs)
        Part 4 Obligations and Contraventions
        Part 5 Other Provisions Relating to the CRO
        Part 6 Application to the QFC Civil and Commercial Court
        Part 7 Reporting
        Part 8 Register of Financing Statements
        Part 9 Acquisition of Minorities in Take-Overs
        Part 10 Interpretation and Definitions
        Schedule 1 Contraventions with Financial Penalties Stipulated
        Schedule 2 Financing Change Statement
    Limited Liability Partnerships Regulations 2005
        Enactment Notice
        Part 1: Application, Commencement and Interpretation
        Part 2: Establishment and Corporate Capacity
        Part 3: Incorporation and Registration
        Part 4: Membership
        Part 5: Names and Change of Names
        Part 6: Registered Office
        Part 7: Formalities of Carrying on Business
        Part 8: Annual Return
        Part 9: Accounting and Audit Requirements
        Part 10: Branches
        Part 11: Contraventions
        Part 12: Other powers of the CRO
        Part 13: Application to Tribunal
        Part 14: Register of Financing Statements
        Part 15: Interpretation and Definitions
        Schedule 1: Contraventions with Financial Penalties Stipulated
        Schedule 2: Financing Change Statement
    Netting Regulations 2017
        Enactment Notice
        1 Citation
        2 Application
        3 Commencement
        4 Authoritative Text of these Regulations
        5 Power of QFC Authority and Regulatory Authority to make Rules
        6 Interpretation
        7 Conflict with other Regulations
        8 Definitions
        9 Qualified Financial Instruments
        10 Designation of Kinds of Instrument as Qualified Financial Instruments
        11 Enforceability of Qualified Financial Instruments that are Gambling Contracts etc
        12 Enforceability of Qualified Financial Instruments Formerly Thought to be Shari'a-Compliant
        13 Enforceability of Netting Agreements — General Rule
        14 Effectiveness of Netting Obligations to make Payments or Deliveries
        15 Limitation on Powers of a Liquidator
        16 Limitation of Insolvency Laws Prohibiting Set-Off
        17 Liquidator's Powers in Relation to Preferences and Fraudulent Transfers
        18 Pre-emption
        19 Realisation, Appropriation and Liquidation of Collateral
        20 Scope of these Regulations
        21 Governing Law of Netting Agreements
    Partnership Regulations 2007
        Enactment Notice
        Part 1: Application, Commencement and Interpretation
        Part 2: Introductory Provisions
        Part 3: Provisions Relating to Partnerships in General
        Part 4 Limited Partnerships
        Part 5 Incorporation and Registration of Limited Partnerships
        Part 6: Limited Partnerships: Annual Return
        Part 7: Limited Partnerships: Accounting and Audit Requirements
        Part 8: Branches
        Part 9: Contraventions
        Part 10: Other Powers of the CRO
        Part 11: Application to the Tribunal
        Part 12: Insolvency Provisions
        Part 13 Interpretation and Definitions
        Schedule 1: Contraventions with Financial Penalties Stipulated
    QFC Authority Regulations
        Enactment Notice
        Part 1: Application, Commencement and Interpretation
        Part 2: The QFC Authority
        Part 3: Permitted Activities
        Part 4: Licensing Requirements and Process
        Part 5: Miscellaneous
        Part 6: Interpretation and Definitions
    QFC Real Estate Ownership Regulations
        Enactment Notice
        Article 1 - CITATION
        Article 2 - APPLICATION
        Article 3 - COMMENCEMENT
        Article 4 - DEFINITIONS
        Article 5
        Article 6
        Article 7
        Article 8
        Article 9
        Article 10
        Article 11
        Article 12
        Article 13
        Article 14
        Article 15
    QFC Tax Regulations
        Enactment Notice
        Part 1: Application, Commencement and Interpretation
        Part 2: General Scheme of Taxation, Residence and the Charge to Tax
        Part 3: Accounting Profit and Accounting Periods
        Part 4: Computational Provisions
        Part 5: Loss Relief
        Part 6: Double Taxation Relief
        Part 7: Reorganisations and Reconstructions
        Part 8: Transfer Pricing
        Part 9: General Partnerships and Limited Partnerships
        Part 10: Limited Liability Partnerships
        Part 11: Islamic Finance
        Part 12: Participation Exemptions
        Part 13: Insurance Companies
        Part 14: Special Exemptions
        Part 15: Concessionary Rate
        Part 16: Credit for Tax Losses
        Part 17: Administration
        Part 18: Rulings by Tax Department
        Part 19: Records and Returns
        Part 20: Enquiries
        Part 21: Assessments
        Part 22: Appeals
        Part 23: Information Powers
        Part 24: Payment and Recovery
        Part 25: Financial Sanctions
        Part 26: Miscellaneous and Supplemental
        Part 27: Interpretation and Definitions
    Security Regulations
        Enactment Notice
        Part 1: Application, Commencement and Interpretation
        Part 2: Effectiveness and Attachment of Security Interest
        Part 3: Perfection
        Part 4: Taking Personal Property Free of Security Interests
        Part 5: Priority
        Part 6: Rights and Remedies on Default
        Part 7: Supplementary Provisions
        Part 8: Interpretation and Definitions
        Schedule 1: Order of Priority for Distribution of Surplus
    Single Family Office Regulations
        Enactment Notice
        Article 1 — Citation
        Article 2 — Application
        Article 3 — Commencement
        Article 4 — Language
        Article 5 — Interpretation
        Article 6 — Power of the QFC Authority and the Regulatory Authority toMake Rules
        Article 7 — Interaction with the QFC Law, QFC Regulations and Rules
        Article 8 — Single Family
        Article 9 — Single Family Office
        Article 10 — Family Member
        Article 11 — Family Fiduciary Structure
        Article 12 — Family Entity
        Article 13 — Scope of Services
        Article 14 — Control
        Article 15 — Establishing a Single Family Office
        Article 16 — Registration
        Article 17 — Non-QFC Entities
        Article 18 — Letter from an Eligible Firm
        Article 19 — Articles of Association
        Article 20 — Designated Representative
        Article 21 — Role of Designated Representative
        Article 22 — Annual Report
        Article 23 — Due Diligence Measures
        Article 24 — Anti-Money Laundering
        Article 25 — Variation or Withdrawal of Licence
        Article 26 — Change of Status
        Article 27 — Duty to Provide Information
        Article 28 — Contraventions
        Article 29 — Prescribed Forms and Prescribed Fees
        Article 30 — Fines
        Article 31 — Interpretation
        Article 32 — Definitions
        Schedule 1 [Deleted]
    Special Company Regulations
        Enactment Notice
        Part 1: General Provisions
        Part 2: Special Purpose Company
        Part 3: Holding Company
        Part 4: Provisions Applicable to both a Special Purpose Company and a Holding Company
        Part 5: Interpretation and Definitions
        Schedule 1 — Fines for Contraventions
    Trust Regulations 2007
        Enactment Notice
        Part 1: Introduction
        Part 2: General
        Part 3: Choice of Governing Law; Place of Administration
        Part 4: Judicial and Non Judicial Proceedings
        Part 5: Creation, Validity and Modification of a QFC Trust
        Part 6: The Beneficiaries of a QFC Trust
        Part 7: Protective Trusts and Creditors' Claims
        Part 8: Office of Trustee
        Part 9: Duties and Powers of Trustees
        Part 10: Liability of Trustees and Rights of Persons Dealing with a Trustee
        Part 11A: The Protector
        Part 11B: The Settlor
        Part 12: Provisions Applicable to a Foreign Trust
        Part 13: Provisions Relating to QFC Authority and the Regulatory Authority
        Part 14: Powers of the QFC Court
        Part 15: Interpretation and Definitions

